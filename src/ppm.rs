pub mod ppm {
    use crate::vec3::Vec3;
    use crate::ray::Ray;
    use std::fs::File;
    use std::io::prelude::*;

    fn dot_product(u: Vec<f32>, v: Vec<f32>) -> f32 {
        assert_eq!(u.len(), v.len());
        u.iter().zip(v.iter()).map(|(x, y)| x * y).sum()
    }

    fn norm(v: &Vec3) -> Vec3 {
        //  w = v * rsq(v.dot(v)) reciprocal-square root v·v
        let x = vec![v.x(), v.y(), v.z()];
        let y = x.clone();
        *v * (1.0 / (dot_product(x, y)).sqrt())
    }

    /* Gradient background to make sure things work */
    fn color(r: Ray) -> Vec3 {
        let dir = r.direction();
        let unit_dir = norm(dir);
        let t: f32 = (unit_dir.y() + 1.0) * 0.5;
        Vec3::new(1.0, 1.0, 1.0) * (1.0 - t) + (Vec3::new(0.5, 0.7, 1.0) * t)
    }

    pub fn print_img() -> std::io::Result<()> {
        let mut file = File::create("foo.ppm")?;

        let nx = 200;
        let ny = 100;
        write!(file, "P3\n{:?} {:?}\n255\n", nx, ny)?;
        // Shoots a ray over a 4.0x2.0 "texture"
        let lower_left = Vec3::new(-2.0, -1.0, -1.0);
        let horizontal = Vec3::new(4.0, 0.0, 0.0);
        let vertical = Vec3::new(0.0, 2.0, 0.0);
        let origin = Vec3::new(0.0, 0.0, 0.0);
        for j in (0..ny).rev() {
            for i in 0..nx {
                let u: f32 = i as f32 / nx as f32;
                let v: f32 = j as f32 / ny as f32;
                let r: Ray = Ray::new(&origin, &(lower_left + horizontal * u + vertical * v));
                let col: Vec3 = color(r);
                let ir: i32 = (col.x() * 255.99f32) as i32;
                let ig: i32 = (col.y() * 255.99f32) as i32;
                let ib: i32 = (col.z() * 255.99f32) as i32;

                write!(file, "{:?} {:?} {:?}\n", ir, ig, ib)?;
            }
        }
        Ok(())
    }
}
